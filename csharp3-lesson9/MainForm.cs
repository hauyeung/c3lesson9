﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace csharp3_lesson9
{
    public partial class MainForm : Form
    {
        
        private TargetGame _targetGame;
        private int _hits = 0;
        private int _level = 1;
        public static int time = 11;
        public MainForm()
        {
            InitializeComponent();
            gameTimer.Start();
            gameTimer.Interval = 50;
            this.pauseToolStripMenuItem.Click += (obj, e) =>
            {
                if (gameTimer.Enabled)
                {
                    gameTimer.Stop();
                    pauseToolStripMenuItem.Text = "Resume";
                }
                else
                {
                    gameTimer.Start();
                    pauseToolStripMenuItem.Text = "Pause";
                }
                
            };
            _targetGame = new TargetGame(boardSize: mainPictureBox.ClientSize);
            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _targetGame.Reset();
            _targetGame.scountdown = 10;
        }
        private int _hitsPerLevel = 4;


        private void MainForm_Shown(object sender, EventArgs e)
        {
            // Set up status
            updateHitsStatus();

            // Set cursor
            mainPictureBox.Cursor = Cursors.Cross;

            // Lambda statement for game reset
            resetToolStripMenuItem.Click += (from, ea) =>
            {
                _hits = 0;
                _level = 1;
                _targetGame.NumberOfTargets = 1;
                updateHitsStatus();
                _targetGame.Reset();
            };

            // Exit event lambda expressions
            exitToolStripMenuItem.Click += (from, ea) => this.Close();

            // PictureBox Mouseup lambda expression
            mainPictureBox.MouseUp += (from, ea) => _targetGame.Select(ea.Location);

            // Create target game object

           // _targetGame = new TargetGame(boardSize: mainPictureBox.ClientSize, countdown: 11 - _level);
            

            // Lambda delegates for Hit
            if (gameTimer.Enabled)
            {
                _targetGame.Hit += (from, ea) => ++_hits;
            }
            _targetGame.Hit += (from, ea) =>
            {
              
                if (_hits == _hitsPerLevel * _targetGame.NumberOfTargets)
                {                    
                    _hits = 0;                    
                    _level++;
                    _targetGame.NumberOfTargets++;
                    if (_targetGame.scountdown > 2)
                    {
                        _targetGame.scountdown--;
                    }
                    Target.numtargets++;
                    if (gameTimer.Interval > 50)
                    {
                        //gameTimer.Interval = 1000 - _targetGame.NumberOfTargets * 150;
                    }
                }

            };
            _targetGame.Hit += (from, ea) => updateHitsStatus();

            // Set up Picturebox Paint event
            mainPictureBox.Paint += (from, ea) => _targetGame.Update(ea.Graphics);

            // Set up Timer Tick event
            gameTimer.Tick += (from, ea) => mainPictureBox.Invalidate();
            TargetGame.TimerExpired += (obj, ev) =>
            {
                _hits = 0;
                hitsToolStripStatusLabel.Text = "Level: " + _level.ToString() + " - Hits: " + _hits.ToString();
            };
        }



        private void updateHitsStatus()
        {
            // Update hits status
            hitsToolStripStatusLabel.Text = "Level: " + _level.ToString() + " - Hits: " + _hits.ToString();

        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Close();
        }


    }

    }

